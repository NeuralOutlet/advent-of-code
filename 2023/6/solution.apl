⍝ The data set is tiny so let's just
times ← 56 71 79 99
records ← 334 1135 1350 2430

⍝ Every element of the 'times' array
⍝ will be tested against the same inputs
⍝ so here we check what the maximum time
⍝ is then make a list of 1 2 3...max
holdcounts←⍳⌈/times

⍝ join the result of testrecord X for
⍝ each element in holdcount
results ← ,testrecord¨holdcounts

⍝ sum up all the results, this leaves us
⍝ with a boxed array aligned with 'times'
⍝ so we take-first to get the array then
⍝ multiply those four elements to finish.
×/↑(+/results)
