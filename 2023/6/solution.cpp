#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
typedef long long llint;

llint winnerCount(llint time, llint record)
{
	llint winners = 0;
	for (llint attempt = 0; attempt < time; ++attempt)
	{
		if ((time - attempt) * attempt > record)
			++winners;
	}
	
	return winners ? winners : 1;
}

vector<llint> parsevalues(string line, bool concat = false)
{
	vector<llint> results;
	string num;
	bool foundNum = false;
	for (int i = 0; i < line.size(); ++i)
	{
		if (isdigit(line[i]))
		{
			foundNum = true;
			num += line[i];
		}
		else
		{
			if (!num.empty() && !concat)
			{
				results.push_back(stoll(num));
				num.clear();
			}
		}
	}
	
	if (!num.empty())
		results.push_back(stoll(num));
	
	return results;
}	

int main()
{
	string line;
	ifstream file("input.txt");
	
	getline(file, line);
	auto times = parsevalues(line, true);
	
	getline(file, line);
	auto distances = parsevalues(line, true);
	
	llint result = 1;
	for (int i = 0; i < times.size(); ++i)
	{
		result *= winnerCount(times[i], distances[i]);
	}
	
	cout << result << endl;
}
