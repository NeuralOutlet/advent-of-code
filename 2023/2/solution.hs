import System.IO
import Control.Monad

validate :: [Int] -> Bool
validate [r,g,b]
    | r > 12 = False
    | g > 13 = False
    | b > 14 = False
    | otherwise = True

-- get rid of the star part 'game XX:'
stripdata :: String -> String
stripdata "" = ""
stripdata (c:cs) = if c == ':' then tail cs else stripdata cs

-- use list comprehension to remove comma and semi-colon
cleandata :: String -> String
cleandata s = [ c | c <- s, c /= ',', c /= ';' ]

-- the meat of it, tally red green and blue seperately
tallyrgb :: [String] -> [Int] -> [Int]
tallyrgb [] rgb = rgb
tallyrgb (n:c:other) (r:g:b:_)
    | c == "red" = tallyrgb other [max r num, g, b]
    | c == "green" = tallyrgb other [r, max g num, b]
    | c == "blue" = tallyrgb other [r, g, max b num]
    | otherwise = tallyrgb other [r,g,b] -- this shouldn't happen tho
    where num = read n :: Int -- like a let scope in lisp i guess

-- put this long sequence into a function to keep it short
parseData :: String -> [Int]
parseData s = tallyrgb (words (cleandata (stripdata s))) [0,0,0]

-----------------------------------------------------------------------
-- Part 1: Get the maximum for each colour and if they are less than
--         the colour limit then it is a valid game. Sum the IDs of
--         all the valid games.

-- create the index by a running tally as we validate the games
sumGames :: [[Int]] -> Int -> Int -> Int
sumGames [] _ s = s
sumGames (g:xg) c s
    | validate g = sumGames xg (c+1) (s+c)
    | otherwise = sumGames xg (c+1) s

-----------------------------------------------------------------------
-- Part 2: Get the minimum for each colour and multiply all colour mins
--         and sum these for the answer

sumGames2 :: [[Int]] -> Int -> Int
sumGames2 [] n = n
sumGames2 (x:xs) n = sumGames2 xs (n + (product x))

main :: IO ()
main = do
    stuff <- readFile "input.txt"
    let games = map parseData (lines stuff)

    print $ sumGames2 games 0
