#include <iostream>
#include <iterator>
#include <fstream>
#include <string>

using namespace std;

// we know the data size is constant
const int width = 140; // test data is 10x10
const int height = 140;

void readInArray(fstream file, char data[][width])
{
	int row = 0, col = 0;
	typedef std::istreambuf_iterator<char> cIter;
	for (cIter iter(file), e; iter != e; ++iter)
	{
		const char c = *iter;
		if (c == '\n') {
			++row;
			col = 0;
			continue;
		}

		data[row][col++] = c;
	}
}


// checks the Moore neighbourhood for symbols,
// returning true if the character is a part symbol
bool checkValid(int i, int j, char data[][width])
{
	// returns false when out of bounds otherwise
	auto safeCheck = [&data](int i, int j)
	{
		if (i < 0 || i >= height || j < 0 || j > width) return false;
		return !(data[i][j] == '.' || std::isdigit(data[i][j]));
	};

	return
		safeCheck(i-1,j-1) || safeCheck(i-1,j) || safeCheck(i-1, j+1) ||
		safeCheck(i  ,j-1) || safeCheck(i  ,j) || safeCheck(i  , j+1) ||
		safeCheck(i+1,j-1) || safeCheck(i+1,j) || safeCheck(i+1, j+1);
}

// walk through the 2D array parsing numbers
// but only add them to the result if they
// were flagged as a part number
int parseAndSumNumbers(char data[][width])
{
	bool isPart = false;
	string num = "";
	int result = 0;

	for (int i = 0; i < height; ++i)
	{
		for (int j = 0; j < width; ++j)
		{
			const char ch = data[i][j];

			if (isdigit(ch))
			{
				isPart = isPart || checkValid(i, j, data);
				num += ch;
			}
			else
			{
				if (isPart)
				{
					result += stoi(num);
				}

				num.clear();
				isPart = false;
			}
		}
	}

	return result;
}

int main()
{
	char data[height][width];
	readInArray(fstream("BullseyeCoverageError.txt"), data);
	cout << parseAndSumNumbers(data) << endl;
}
