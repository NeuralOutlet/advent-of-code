import System.IO
import Control.Monad
import Data.Char

-- 10 for p1 test data, 140 for p1 actual data
offset = 140

validate :: Maybe Char -> Bool
validate c = case c of
    Nothing -> False -- no effect
    Just ch -> if (ch == '.') || (isDigit ch)
        then False else True

-- safe indexing for if there may not be items there
(!?) :: String -> Int -> Maybe Char
s !? i = if i >= (length s) then Nothing else Just (s !! i)

-- a 3x3 neighbourhood check
-- past := the current char and all that came before
-- future := the current char and all that is to come
validateArea :: String -> String -> Bool
validateArea past future =
       validate (past !? (offset))
    || validate (past !? (offset + 1))
    || validate (past !? (offset - 1))
    || validate (past !? 1)
    || validate (future !? 1)
    || validate (future !? (offset))
    || validate (future !? (offset + 1))
    || validate (future !? (offset - 1))

appendnum :: [Int] -> String -> Bool -> [Int]
appendnum nums num good
    | good && num /= "" = nums ++ [read num :: Int]
    | otherwise = nums

-- keep the history by reversing the string as we fold over it
-- and then check the offset distance (line length) and either side
-- of that to fully check a 3x3 area in the data. If at any point
-- while constructing a number it becomes valid then keep it.
collectnums :: String -> String -> String -> Bool -> [Int] -> [Int]
collectnums _ "" num good nums = if num /= "" then (appendnum nums num good) else nums
collectnums past (c:cs) num good nums
    | isDigit c = collectnums ([c] ++ past) cs (num ++ [c]) isValid nums
    | otherwise = collectnums ([c] ++ past) cs "" False (appendnum nums num good)
    where isValid = ((validateArea ([c] ++ past) ([c] ++ cs)) || good)

-- get rid of "\n"
parseData :: [String] -> String -> String
parseData [] output = output
parseData (s:xs) output = parseData xs (output ++ s)

main :: IO ()
main = do
    stuff <- readFile "BullseyeCoverageError.txt"

    -- had to parse like this because '\' and 'n' were
    -- appearing in the table as symbols
    let datum = parseData (lines stuff) ""

    let numparts = collectnums "" datum "" False []
    print $ sum numparts

