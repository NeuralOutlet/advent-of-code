import System.IO
import Control.Monad
import GHC.Read
import Text.Read

-- function to parse each number in the string
-- leaving us with a list of all the numbers
numlist :: String -> [Int]
numlist "" = []
numlist ('o':'n':'e':cs)         = 1 : numlist ('e':cs)
numlist ('t':'w':'o':cs)         = 2 : numlist ('o':cs)
numlist ('t':'h':'r':'e':'e':cs) = 3 : numlist ('e':cs)
numlist ('f':'o':'u':'r':cs)     = 4 : numlist ('r':cs)
numlist ('f':'i':'v':'e':cs)     = 5 : numlist ('e':cs)
numlist ('s':'i':'x':cs)         = 6 : numlist ('x':cs)
numlist ('s':'e':'v':'e':'n':cs) = 7 : numlist ('n':cs)
numlist ('e':'i':'g':'h':'t':cs) = 8 : numlist ('t':cs)
numlist ('n':'i':'n':'e':cs)     = 9 : numlist ('e':cs)
numlist (c:cs) =
    case (readMaybe [c] :: Maybe Int) of
        Just x -> x : numlist cs
        Nothing -> numlist cs

-- instead of trying to concat to Ints and having type struggles
-- we can just multiply the first digit by ten as it's only ever
-- two digit numbers, so we map all our lines of text to this
sumsatenate :: String -> Int
sumsatenate "" = 0
sumsatenate s = 10 * (head $ numlist s) + (last $ numlist s)


main :: IO ()
main = do
    stuff <- readFile "input.txt"
    let jumbles = lines stuff
    let cvalues = map sumsatenate jumbles
    print $ sum cvalues
