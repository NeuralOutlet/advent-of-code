#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

struct Datum
{
	long long value;
	enum { Waiting = 0, Processed = 1} state;
};

bool initMapValues(string line, long long &src, long long &dst, long long &len)
{
	return (sscanf(line.c_str(), "%lld %lld %lld", &dst, &src, &len) == 3);
}

int main()
{
	ifstream file("input.txt");
	vector<Datum> data;
	stringstream ss;
	string line;

	getline(file, line);
	string item;
	ss << line;

	// parse the initial seed line
	for (long long num; getline(ss, item, ' ');)
	{
		if (stringstream(item) >> num)
		{
			data.push_back({num, Datum::Waiting});
		}
	}

	long long dst = 0, len = 0, src = 0;
	for (std::string line; getline(file, line, '\n');)
	{
		// if this fails then we are changing to the next map
		if (initMapValues(line, src, dst, len))
		{
			for (auto &num : data)
			{
				if (num.state == Datum::Waiting)
				{
					if (num.value >= src && num.value < (src + len))
					{
						num.value = dst + (num.value - src);
						num.state = Datum::Processed;
					}
				}
			}
		}
		else
		{
			if (line.length() <= 1)
			{
				for (auto &num : data)
				{
					num.state = Datum::Waiting;
				}
			}
		}
	}

	long long minval = numeric_limits<long long>::max();
	for (auto &num : data)
	{
		if (num.value < minval)
		{
			minval = num.value;
		}
	}
	cout << "\n" << minval << endl;
}
