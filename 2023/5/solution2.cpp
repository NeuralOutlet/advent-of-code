#include <functional>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;
using namespace std::chrono;

typedef long long int llint;
typedef function<bool(llint&)> mapfunc;

mutex g_mutex;

bool initMapValues(string line, llint &src, llint &dst, llint &len)
{
	return (sscanf(line.c_str(), "%lld %lld %lld", &dst, &src, &len) == 3);
}

vector<llint> parseSeeds(string line)
{
	stringstream ss;
	string item;
	ss << line;

	vector<llint> output;
	for (;getline(ss, item, ' ');)
	{
		llint input;
		if (stringstream(item) >> input)
		{
			output.push_back(input);
		}
	}

	return output;
}

// parseMap takes each line of the mapping block in
// the almanac and creates a lamdba for mapping from
// source to destination ranges. The lambda first calls
// a copy of its previous definition so one lambda can
// be built up over each line of the mapping block
//
// returns true if a map was created and replaces 'datamap'
bool parseMap(ifstream &file, mapfunc &datamap)
{
	bool foundMapBlock = false;
	llint dst = 0, len = 0, src = 0;
	for (std::string line; getline(file, line, '\n');)
	{
		// if this fails then we are changing to the next map
		if (initMapValues(line, src, dst, len))
		{
			foundMapBlock = true;

			// redefine our lambda to attempt previous
			// mapping and if unsuccessful continue to
			// the mapping for this line of the block
			datamap = [datamap, src, dst, len](llint &input)
			{
				if (datamap(input)) return true;
				if (input >= src && input < (src + len)) {
					input = dst + (input - src);
					return true;
				} else return false;
			};
		}
		else
		{
			// end of mapping block
			if (foundMapBlock) return true;
		}
	}

	return foundMapBlock;
}


// callable version of getting the minimum location value of a range
void applyMaps(vector<mapfunc> datamaps, llint start, llint length, llint &finalminimum)
{
	llint minimum = numeric_limits<llint>::max();
	llint end = (start + length);
	for (llint seed = start; seed < end; ++seed)
	{
		llint temp = seed;
		for (int m = 0; m < datamaps.size(); ++m)
		{
			datamaps[m](temp);
		}
		minimum = min(minimum, temp);
	}

	lock_guard<mutex> guard(g_mutex);
	finalminimum = min(finalminimum, minimum);
	cout << "thread " << this_thread::get_id() << ", range " << start << "->" << end << " complete: " << minimum << endl;
}

int main()
{
	ifstream file("input.txt");
	string seedline;
	getline(file, seedline);

	// we will read the first line using 'parseSeeds'
	// to store our seed ranges for later
	auto seeddata = parseSeeds(seedline);

	// now we read the rest of the file using 'parseMap'
	// which won't fail again until there are no maps found
	bool moreMaps = true;
	vector<mapfunc> datamaps;
	while (moreMaps) {
		// our identity lambda to start our chain
		mapfunc datamap = [](llint &input) { return false; };
		if (moreMaps = parseMap(file, datamap))
		{
			datamaps.push_back(datamap);
		}
	}

	auto timerStart = high_resolution_clock::now();

	// Now we're gunna go through our seed ranges
	// and apply all the maps to each seed while
	// recording the minimum location factor
	vector<thread> datathreads;
	llint finalminimum = numeric_limits<long long>::max();
	for (int i = 0; i < seeddata.size(); i+=2)
	{
		datathreads.push_back(thread(applyMaps, datamaps,
		                             seeddata[i],seeddata[i+1],
		                             ref(finalminimum)));
	}
	
	for (auto &dt : datathreads) dt.join();
	cout << "\nMinimum location number: " << finalminimum << endl;

	auto timerStop = high_resolution_clock::now();
	auto time = duration_cast<microseconds>(timerStop - timerStart);
	cout << time.count() << "μs" << endl;
}
