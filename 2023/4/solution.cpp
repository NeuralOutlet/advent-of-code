#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_set>

using namespace std;

// There are always a set amount of winners
// so we can seperate them out by an offset
const int offset = 12;

int main()
{
	ifstream inputFile("input.txt");
	int result = 0;

	/*
	 * Read in each line, use stringstream to read
	 * in each of the numbers in the line, discarding
	 * the first as it is the card ID. While reading
	 * in the numbers put the first N numbers into a
	 * winner's map and afterwards check new numbers
	 * to see if they appear in the map of winners.
	*/

	string line;
	while (getline(inputFile, line)) // per game
	{
		unordered_set<int> winners;
		stringstream ss;
		ss << line;
		
		string item;
		int gamesum = 0;
		for (int numID = 0; getline(ss, item, ' ');)
		{
			int num = 0;
			if (stringstream(item) >> num)
			{
				if (numID++ == 0) continue;
			
				if (numID < offset)
				{
					winners.insert(num);
				}
				else
				{
					int scalar = (1 + winners.count(num));
					if (gamesum == 0 && scalar == 2) {
						gamesum = 1;
					} else {
						gamesum *= scalar;
					}
				}
			}
		}
		result += gamesum;
	}

	cout << result << endl;
}
