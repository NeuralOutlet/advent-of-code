
import System.IO
import Control.Monad

-- function that groups and sums triples into a list
tripsum :: [Integer] -> [Integer]
tripsum (x:y:z:xs) = (x+y+z) : tripsum (y:z:xs)
tripsum _         = []

-- mask list: check if value in list is greater than it's
-- predecessor output is a bool mask of 1 if it is, 0 if not
succmask :: [Integer] -> [Bool]
succmask [] = []
succmask [x] = []
succmask (x:y:xs) = (x < y) : succmask (y:xs)

-- running tally, adding 1 for each true in the mask list
sumbools :: [Bool] -> Integer -> Integer
sumbools [] y = y
sumbools (True:xs) y  = sumbools xs (y+1)
sumbools (False:xs) y = sumbools xs y

main = do
	stuff <- readFile "C:/advent_of_code/input.txt"
	let numbers = map read (lines stuff) :: [Integer]
	let triples = tripsum numbers
	let succnums = succmask triples
	let othernums = sumbools succnums 0
	print othernums
