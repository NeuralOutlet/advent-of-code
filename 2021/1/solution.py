from functools import reduce

text = open('input.txt', 'r')
nums = text.readlines()
nums = list(map(int, nums))

# --- remove this section for part I ---
numtriples = []
for i in range(len(nums) - 2):
    triple = nums[i] + nums[i+1] + nums[i+2]
    numtriples.append(triple)
nums = numtriples
# ---------------------------------------

weights = [k < v for k,v in zip(nums[:-1], nums[1:])]

answer = reduce(lambda a,b:a+b, weights)

print(answer)
