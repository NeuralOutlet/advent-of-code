⍝ Day 1, part 1:
data ← ⍎⍤1⊢↑ ⊃⎕NGET ('C:\advent_of_code\', 'input.txt') 1
+/(¯1↓data)<(1↓data)
	
⍝ Day 1, part 2:
data ← ⍎¨⊃⎕NGET ('C:\advent_of_code\', 'input.txt') 1
data ← 3+/data
+/(¯1↓data)<(1↓data)
	
	