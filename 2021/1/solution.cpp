#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>

using namespace std;

int main()
{
    ifstream infile("C:\\advent_of_code\\input.txt", ios::in);
    istream_iterator<int> start(infile), end;
    vector<int> numbers(start, end);

	vector<int> triples;
    for (int i = 0; i < numbers.size() - 2; ++i)
    {
        int triple = numbers[i] + numbers[i+1] + numbers[i+2];
        triples.push_back(triple);
    }

	int sum = 0;
	for(int i = 0; i < triples.size() - 1; ++i)
		sum += triples[i] < triples[i+1];

	cout << sum << "\n";
    return 0;
}
