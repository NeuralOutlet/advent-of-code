import System.IO
import Control.Monad


takeTail :: [a] -> a
takeTail container = (head (tail container))

parseCell :: [String] -> (Char, Int)
parseCell cell = (head (head cell), (read (takeTail cell) :: Int))

foldPairs :: [(Char, Int)] -> Int -> Int -> (Int, Int)
foldPairs (x:xs) hsum vsum = case (fst x) of
    'f' -> foldPairs xs (hsum + (snd x)) vsum
    'u' -> foldPairs xs hsum (vsum - (snd x))
    'd' -> foldPairs xs hsum (vsum + (snd x))
foldPairs _ hsum vsum = (hsum, vsum)

foldPairs2 :: [(Char, Int)] -> Int -> Int -> Int -> (Int, Int)
foldPairs2 (x:xs) hsum vsum aim = case (fst x) of
    'f' -> foldPairs2 xs (hsum + (snd x)) (vsum + (aim * (snd x))) aim
    'u' -> foldPairs2 xs hsum vsum (aim - (snd x))
    'd' -> foldPairs2 xs hsum vsum (aim + (snd x))
foldPairs2 _ hsum vsum _ = (hsum, vsum)

main = do
    stuff <- readFile "input.txt"
    let output = map words (lines stuff) :: [[String]]
    let parsed = map parseCell output :: [(Char, Int)]
    let result = foldPairs2 parsed 0 0 0
    print ((fst result) * (snd result))
